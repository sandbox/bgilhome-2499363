<?php

function commerce_sauce_settings() {
  $form = array();

  // Price options
  $form['price'] = array(
    '#type' => 'fieldset',
    '#title' => t('Price options'),
  );
  $form['price']['cs_hide_aud'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide AUD code on prices'),
    '#default_value' => variable_get('cs_hide_aud', TRUE),
  );
  $form['price']['cs_hide_zero_decimals'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide decimals if zero'),
    '#default_value' => variable_get('cs_hide_zero_decimals', FALSE),
  );
  $form['price']['cs_price_widget_format'] = array(
    '#type' => 'checkbox',
    '#title' => t('Format price field widgets using currency info settings (for symbol/code placement)'),
    '#default_value' => variable_get('cs_price_widget_format', TRUE),
  );

  // Order options
  $form['order'] = array(
    '#type' => 'fieldset',
    '#title' => t('Order options'),
  );
  $form['order']['cs_order_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add email to billing profile'),
    '#default_value' => variable_get('cs_order_email', TRUE),
  );
  $form['order']['cs_anon_account'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create account for anonymous user'),
    '#default_value' => variable_get('cs_anon_account'),
  );

  // Checkout options
  $form['checkout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Checkout options'),
  );
  $form['checkout']['cs_skip_empty_checkout'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip empty checkout panes'),
    '#default_value' => variable_get('cs_skip_empty_checkout', TRUE),
  );
  $form['checkout']['cs_hide_payment_options'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide payment method options if only one'),
    '#default_value' => variable_get('cs_hide_payment_options', TRUE),
  );
  $form['checkout']['cs_checkout_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Extra checkout pages'),
    '#default_value' => variable_get('cs_checkout_pages'),
  );

  // Line item options
  $form['line_item'] = array(
    '#type' => 'fieldset',
    '#title' => t('Line item options'),
  );
  $form['line_item']['cs_quantity_widget'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity widget'),
    '#description' => t('Machine name of widget to use for line item quantity fields. Must be a textfield widget. NB this is only for line item widgets, not eg. add to cart form.'),
    '#default_value' => variable_get('cs_quantity_widget'),
  );

  // Form options
  $form['form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form options'),
  );
  $form['form']['cs_product_ref_label_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Format string for field/widget label titles on product reference fields (or entityreference fields with product target bundles)'),
    '#description' => t('Leave empty for default behaviour. String can include HTML, and placeholders: !title, !price. If also using Theme Sauce, !field placeholder can also be used.'),
    '#default_value' => variable_get('cs_product_ref_label_format'),
  );

  $form['form']['cs_total_forms'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('AJAX-calculate total on the following forms'),
    '#description' => t('Form IDs, one per line. Form can already contain field_total, or will be appended. Fields to be totalled can be auto-gathered from available price / product reference / entityreference fields, or specified below.'),
    '#default_value' => variable_get('cs_total_forms'),
    '#ajax' => array(
      'wrapper' => 'cs-total-fields',
      'callback' => 'cs_total_fields_callback',
    ),
  );
  $form['form']['cs_total_fields'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Total fields'),
    '#description' => t('Optional - specify fields to be used in total calculations.'),
    '#prefix' => '<div id="cs-total-fields">',
    '#suffix' => '</div>',
  );
  $form['form']['cs_order_forms'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Create orders on submitting the following forms'),
    '#description' => t('Form IDs, one per line. Order will be created using available product reference / line item reference / entityreference fields, or fields specified below.'),
    '#default_value' => variable_get('cs_order_forms'),
    '#ajax' => array(
      'wrapper' => 'cs-order-fields',
      'callback' => 'cs_order_fields_callback',
    ),
  );
  $form['form']['cs_order_fields'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Order fields'),
    '#description' => t('Optional - specify fields to be used in creating order.'),
    '#prefix' => '<div id="cs-order-fields">',
    '#suffix' => '</div>',
  );
  $form['form']['cs_order_redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect to checkout after order creation'),
    '#default_value' => variable_get('cs_order_redirect', FALSE),
  );
  // Apply AJAX callbacks on initial render
  $form['form']['cs_total_fields'] = cs_total_fields_callback($form, $form_state);
  $form['form']['cs_order_fields'] = cs_order_fields_callback($form, $form_state);

  // Custom submit
  $form['#submit'][] = 'commerce_sauce_settings_submit';

  return system_settings_form($form);
}

// AJAX callback for cs_total_fields
function cs_total_fields_callback($form, $form_state) {
  $total_forms = empty($form_state['values']) ? variable_get('cs_total_forms') : $form_state['values']['cs_total_forms'];
  $total_forms = explode("\n", $total_forms);
  foreach ($total_forms as $form_id) {
    if (!isset($form['form']['cs_total_fields']['cs_total_fields_' . $form_id])) {
      $form['form']['cs_total_fields']['cs_total_fields_' . $form_id] = array(
        '#type' => 'textarea',
        '#rows' => 3,
        '#title' => t('Total fields for form @id', array('@id' => $form_id)),
        '#description' => t('Field names, one per line.'),
        '#default_value' => variable_get('cs_total_fields_' . $form_id),
      );
    }
  }

  return $form['form']['cs_total_fields'];
}

// AJAX callback for cs_order_fields
function cs_order_fields_callback($form, $form_state) {
  $order_forms = empty($form_state['values']) ? variable_get('cs_order_forms') : $form_state['values']['cs_order_forms'];
  $order_forms = explode("\n", $order_forms);
  foreach ($order_forms as $form_id) {
    if (!isset($form['form']['cs_order_fields']['cs_order_fields_' . $form_id])) {
      $form['form']['cs_order_fields']['cs_order_fields_' . $form_id] = array(
        '#type' => 'textarea',
        '#rows' => 3,
        '#title' => t('Order fields for form @id', array('@id' => $form_id)),
        '#description' => t('Field names, one per line.'),
        '#default_value' => variable_get('cs_order_fields_' . $form_id),
      );
    }
  }

  return $form['form']['cs_order_fields'];
}

// Custom submit
function commerce_sauce_settings_submit($form, &$form_state) {

  // Disable anon order completion account creation rule
  $rules_config = rules_config_load('commerce_checkout_new_account');
  if ($rules_config) {
    $rules_config->active = $form_state['values']['cs_anon_account'];
    $rules_config->save();
  }

  // Change quantity widgets
  $current_quantity_widget = variable_get('cs_quantity_widget');
  $new_quantity_widget = $form_state['values']['cs_quantity_widget'];
  if ($current_quantity_widget != $new_quantity_widget) {
    // TODO change widget for quantity - problem is it's an extra field
  }
}
